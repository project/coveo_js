(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.coveoSearch = {
    attach: function (context, settings) {
      const elements = once('init-coveo', 'body', context);
      elements.forEach(function () {
        Drupal.behaviors.coveoSearch.initCoveo();
      });
    },
    initCoveo: function () {
      // Default Custom Coveo Parameters.
      Coveo.customParameters = Coveo.customParameters || {
        coveoOrgId: undefined,
        coveoSearchToken: undefined,
        coveoRestURI: undefined,
        subHub: undefined,
        context: {},
        coveoMainSearchRoot: undefined,
        CustomReplaceResultUriLocale: {
          replaceLocale: undefined
        },
        coveoPipelineContext: undefined,
        coveoCustomPipelineContext: undefined,
        coveoStandaloneSearchBoxRoot: undefined,
        coveoSearchPageUrl: undefined
      };
      // Get the search token from the internal endpoint.
      const getToken = function getToken() {
        return fetch('/api/coveo/token').then(function (response) {
          return response.json();
        }).then(function (results) {
          return results.token;
        });
      };

      initializeCoveoCustomSearchInterface = function () {
        // Enable history.
        $(Coveo.customParameters.coveoMainSearchRoot).attr('data-enable-history', true);
        Coveo.init(
          Coveo.customParameters.coveoMainSearchRoot, {
            Analytics: {
              searchHub: (Coveo.customParameters.subHub ? Coveo.customParameters.subHub + '' : '')
            },
            Searchbox: {
              placeholder: Coveo.l('SearchBox_placeholder')
            },
            CustomPipelineContext: {
              context: Coveo.customParameters.context
            },
            CustomReplaceResultUriLocale: {
              replaceLocale: Coveo.customParameters.CustomReplaceResultUriLocale.replaceLocale
            },
            PrintableUri: {
              alwaysOpenInNewWindow: true
            },
            ResultLayoutSelector: {
              desktopLayouts: ['list'],
              tabletLayouts: ['list'],
              mobileLayouts: ['list']
            },
            externalComponents: [Coveo.customParameters.coveoPipelineContext, Coveo.customParameters.coveoCustomPipelineContext, Coveo.customParameters.coveoStandaloneSearchBoxRoot]
          });
      };

      initializeCoveoCustomStandaloneSearchBox = function () {
        Coveo.initSearchbox(
          Coveo.customParameters.coveoStandaloneSearchBoxRoot,
          Coveo.customParameters.coveoSearchPageUrl, {
            Analytics: {
              searchHub: (Coveo.customParameters.subHub ? Coveo.customParameters.subHub + '' : '')
            },
            Searchbox: {
              placeholder: Coveo.l('SearchBox_placeholder')
            },
            CustomPipelineContext: {
              context: Coveo.customParameters.context
            }
          }
        );
      }

      Coveo.customParameters.subHub = drupalSettings.coveo_js.search_hub || '';

      // Default Coveo search URL.
      Coveo.customParameters.coveoSearchPageUrl = '/search';

      Coveo.customParameters.coveoOrgId = drupalSettings.coveo_js.organization_id || '';
      Coveo.customParameters.coveoSearchToken = drupalSettings.coveo_js.search_token.token || getToken();
      Coveo.customParameters.coveoRestURI = 'https://platform.cloud.coveo.com/rest/search';
      Coveo.customParameters.coveoMainSearchRoot = document.querySelector(drupalSettings.coveo_js.search_interface) || undefined;
      Coveo.customParameters.coveoStandaloneSearchBoxRoot = document.querySelector(
        '#coveo_block_standalone_search_box') || undefined;
      Coveo.customParameters.coveoPipelineContext = document.querySelector(
        drupalSettings.coveo_js.search_interface + ' .CoveoPipelineContext') ||
        undefined;
      Coveo.customParameters.coveoCustomPipelineContext = document.querySelector(
        drupalSettings.coveo_js.search_interface + ' .CoveoCustomPipelineContext') ||
        undefined;

      // Configure the Coveo Search Endpoint with the renew token option.
      Coveo.SearchEndpoint.configureCloudV2Endpoint(
        drupalSettings.coveo_js.organization_id,
        drupalSettings.coveo_js.search_token.token,
        "https://platform.cloud.coveo.com/rest/search/",
        {
          renewAccessToken: getToken
        }
      );

      // Standalone Search Box is present but Main Search is not.
      if (!Coveo.customParameters.coveoMainSearchRoot && Coveo.customParameters.coveoStandaloneSearchBoxRoot) {
        initializeCoveoCustomStandaloneSearchBox();
      }
      // Main Search is present but Standalone Search Box is not.
      else if (Coveo.customParameters.coveoMainSearchRoot) {
        initializeCoveoCustomSearchInterface();
      }

      // Initialize culture.
      this.culture();
    },
    culture: function culture() {
      const en_dict = {
        'en': {
          'SearchBox_placeholder': 'Search by Keyword',
          'Quickview': 'Quickview',
          'ReadMore': 'Read More'
        }
      };

      const en_dict_Facet_valueCaption = {
        'en': {}
      };

      String.toLocaleString(en_dict);
      String.toLocaleString(en_dict_Facet_valueCaption);
    }
  };
})(jQuery, Drupal, drupalSettings);

<?php

namespace Drupal\coveo_js\Controller;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Drupal\coveo_js\Client\Coveo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides route responses for the Example module.
 */
class SearchPage extends ControllerBase {

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The Coveo client.
   *
   * @var \Drupal\coveo_js\Client\Coveo
   */
  protected $coveo;

  /**
   * Constructs the Coveo JS search controller.
   *
   *   The plugin implementation definition.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The configuration object.
   * @param \Drupal\coveo_js\Client\Coveo $coveo
   *   The Coveo HTTP client.
   */
  public function __construct(ImmutableConfig $config, Coveo $coveo) {
    $this->config = $config;
    $this->coveo = $coveo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')->get('coveo_js.settings'),
      $container->get('coveo_js.http_client')
    );
  }

  /**
   * Returns a custom Coveo search page.
   *
   * @return array
   *   A search page renderable array.
   *
   * @throws \Exception
   */
  public function search() {
    $build = [
      '#theme' => 'coveo-js-search-page',
      '#cache' => [
        // Hard-code max-age to 23 hours to account for token refreshment needs.
        'max-age' => 82800,
        'contexts' => [
          'url',
        ],
      ],
    ];

    $build['#attached']['library'][] = 'coveo_js/search_ui';
    $build['#attached']['library'][] = 'coveo_js/cultures';
    $build['#attached']['library'][] = 'coveo_js/init';
    $build['#attached']['drupalSettings']['coveo_js']['organization_id'] = $this->config->get('coveo_organization_id');
    $build['#attached']['drupalSettings']['coveo_js']['search_interface'] = $this->config->get('coveo_search_interface');
    $build['#attached']['drupalSettings']['coveo_js']['search_hub'] = $this->config->get('coveo_search_hub');
    $build['#attached']['drupalSettings']['coveo_js']['debug'] = $this->config->get('debug');
    $build['#attached']['drupalSettings']['coveo_js']['search_token'] = $this->coveo->requestSearchToken();

    return $build;
  }

}

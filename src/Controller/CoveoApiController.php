<?php

namespace Drupal\coveo_js\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\coveo_js\Client\Coveo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Coveo API proxy controller.
 */
class CoveoApiController extends ControllerBase {

  /**
   * Construct a new Coveo controller.
   *
   * @param \Drupal\coveo_js\Client\Coveo $coveo
   *   The Coveo client.
   */
  public function __construct(Coveo $coveo) {
    $this->coveo = $coveo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('coveo_js.http_client')
    );
  }

  /**
   * Request a Coveo search token for anonymous users.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json response.
   *
   * @throws \Exception
   */
  public function getSearchToken() {
    $response = $this->coveo->requestSearchToken();
    $response = new JsonResponse($response);
    return $response;
  }

}

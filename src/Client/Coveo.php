<?php

namespace Drupal\coveo_js\Client;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * Processes the services for Coveo JS.
 */
class Coveo {

  use LoggerChannelTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * A configuration object containing coveo_js settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The Coveo endpoint for request a search token.
   *
   * @var string
   */
  protected $coveoEndpoint = 'https://platform.cloud.coveo.com/rest/search/token';

  /**
   * The Coveo minimum token expiration value 15 minutes in milliseconds.
   *
   * @var int
   */
  protected $tokenMin = 900000;

  /**
   * The Coveo maximum/default token expiration value 12 hours in milliseconds.
   *
   * @var int
   */
  protected $tokenMax = 86400000;

  /**
   * Construct a new Coveo_js Guzzle Wrapper.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, CacheBackendInterface $cache, TimeInterface $time) {
    $this->config = $config_factory->get('coveo_js.settings');
    $this->client = $http_client;
    $this->cache = $cache;
    $this->time = $time;
  }

  /**
   * Get the required Search API key.
   *
   * @return string
   *   The Coveo Search API Key.
   */
  private function getSearchApiKey() {
    $key = $this->config->get('coveo_search_api_key');
    if (empty($key)) {
      $this->getLogger('coveo_js')
        ->critical('Coveo Search API key not set on current environment.');
    }
    return $key;
  }

  /**
   * Get the optional Search Hub.
   *
   * @return string
   *   The Coveo Search Hub.
   */
  private function getSearchHub() {
    return $this->config->get('coveo_search_hub');
  }

  /**
   * Get the Token expiration.
   *
   * @return int
   *   The Coveo token expiration in milliseconds.
   */
  public function getTokenExpiration() {
    // Get from the module configuration the value in milliseconds how long
    // should be the token valid.
    // By default, Coveo search tokens automatically expire after 24 hours.
    // Minimum value: 900000 milliseconds (i.e., 15 minutes).
    // Maximum/default: 86400000 milliseconds (i.e., 24 hours).
    // https://docs.coveo.com/en/56/cloud-v2-developers/search-token-authentication#validfor-integer-optional
    $token_expiration = (int) $this->config->get('coveo_token_expiration');
    if ($token_expiration > $this->tokenMax) {
      $token_expiration = $this->tokenMax;
    }
    if ($token_expiration < $this->tokenMin) {
      $token_expiration = $this->tokenMin;
    }

    return $token_expiration;
  }

  /**
   * Set the data for the token request for anonymous users.
   *
   * @return array
   *   The Data to send for a token request.
   */
  private function setRequestBodyPayload() {
    // Add optional parameters to the request body payload.
    // @todo Add other COVEO optional parameters like pipelines, filters, etc.
    $searchHub = $this->getSearchHub();
    if (!empty($searchHub)) {
      $payload['searchHub'] = $searchHub;
    }

    // Set the time the token will remain valid.
    $payload['validFor'] = $this->getTokenExpiration();

    // Add required parameters to the body payload.
    $headers = [
      'Authorization' => 'Bearer ' . $this->getSearchApiKey(),
      'Content-Type' => 'application/json',
    ];
    $payload['userIds'][] = [
      'name' => 'anonymous@coveo.com',
      'provider' => 'Email Security Provider',
    ];
    $request_data = [
      'body' => json_encode($payload),
      'headers' => $headers,
    ];

    return $request_data;
  }

  /**
   * Request and cache a search token to Coveo.
   *
   * @return array
   *   The token from the request response.
   *
   * @throws \Exception
   */
  public function requestSearchToken() {
    $token = NULL;
    $data = $this->setRequestBodyPayload();
    $response = $this->request('POST', $this->coveoEndpoint, $data);
    if ($response && $response->getStatusCode() == '200') {
      $payload = json_decode($response->getBody()->__toString());
      if (isset($payload->token)) {
        $token = $payload->token;
      }
    }

    return ['token' => $token];
  }

  /**
   * Generates an endpoint HTTP request.
   *
   * @param string $method
   *   The request method.
   * @param string $endpoint
   *   The requested endpoint.
   * @param array $data
   *   An array of parameters to pass into the request.
   *
   * @return mixed
   *   The response from the request.
   *
   * @throws \Exception
   */
  private function request($method, $endpoint, array $data = []) {
    try {
      $request = $this->client->{$method}(
        $endpoint,
        $data
      );
    }
    catch (ClientException $e) {
      $this->getLogger('coveo_js')
        ->error('Exception for @uri: @e', [
          '@uri' => $endpoint,
          '@e' => $e->getMessage(),
        ]);
      $request = NULL;
    }

    return $request;
  }

}

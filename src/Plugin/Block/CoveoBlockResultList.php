<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Result List tag.
 *
 * @Block(
 *   id = "coveo_block_result_list",
 *   admin_label = @Translation("Coveo: Result List"),
 * )
 */
class CoveoBlockResultList extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoResultList';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // "list" or "card".
      'data-layout' => "list",
      'data-wait-animation' => "fade",
      'data-auto-select-fields-to-include' => "true",
    ];
  }

}

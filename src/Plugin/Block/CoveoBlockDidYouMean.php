<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Did You Mean tag.
 *
 * @Block(
 *   id = "coveo_block_did_you_mean",
 *   admin_label = @Translation("Coveo: Did You Mean"),
 * )
 */
class CoveoBlockDidYouMean extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoDidYouMean';

}

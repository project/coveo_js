<?php

namespace Drupal\coveo_js\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\coveo_js\Client\Coveo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base block implementation that Coveo blocks can extend.
 *
 * This abstract class provides the generic block configuration form, default
 * block settings, and handling for general user-defined block visibility
 * settings.
 *
 * @ingroup block_api
 */
abstract class CoveoBlockBase extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The Coveo client.
   *
   * @var \Drupal\coveo_js\Client\Coveo
   */
  protected $coveo;

  /**
   * Optional wrapper attributes.
   *
   * @var array
   */
  protected $wrapper = [];

  /**
   * Required block class.
   *
   * @var string
   */
  protected $class = '';

  /**
   * Optional data-attribute keys and values.
   *
   * @var array
   */
  protected $attributes = [];

  /**
   * Constructs a CoveoBlockBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The configuration object.
   * @param \Drupal\coveo_js\Client\Coveo $coveo
   *   Coveo Guzzle client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImmutableConfig $config, Coveo $coveo) {
    $this->config = $config;
    $this->coveo = $coveo;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('coveo_js.settings'),
      $container->get('coveo_js.http_client')
    );
  }

  /**
   * Allows blocks to specify optional configurable data-attributes.
   *
   * @return array
   *   The default configuration.
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Set the block's class.
    if (!empty($this->class)) {
      $this->attributes['class'] = [$this->class];
    }

    // Set the blocks data-attributes.
    $config = $this->getConfiguration();
    if (!empty($config)) {
      foreach ($config as $data_key => $data_attribute) {
        $this->attributes[$data_key] = $data_attribute;
      }
    }

    // Set the main content.
    $build['content'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => $this->attributes,
      '#value' => '<div class="visually-hidden focusable">' . $config['label'] . '</div>',
    ];

    // Set the wrapper.
    if (!empty($this->wrapper)) {
      $nested = $build['content'];
      $build['content'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [],
        '#value' => '<div class="visually-hidden focusable">' . $config['label'] . ' Wrapper' . '</div>',
        'nested' => $nested,
      ];

      if ($this->wrapper['id']) {
        $build['content']['#attributes']['id'] = $this->wrapper['id'];
      }
      if ($this->wrapper['class']) {
        $build['content']['#attributes']['class'] = is_array($this->wrapper['class']) ? $this->wrapper['class'] : explode(' ', $this->wrapper['class']);
      }
    }

    // Append attachments.
    $this->appendAttachments($build);

    $build['#cache'] = [
      'max-age' => $this->coveo->getTokenExpiration(),
    ];

    return $build;
  }

  /**
   * Helper function appends the necessary attachments to renderable arrays.
   */
  protected function appendAttachments(array &$build) {
    $build['#attached']['library'][] = 'coveo_js/search_ui';
    $build['#attached']['library'][] = 'coveo_js/cultures';
    $build['#attached']['library'][] = 'coveo_js/init';
    $build['#attached']['drupalSettings']['coveo_js']['organization_id'] = $this->config->get('coveo_organization_id');
    $build['#attached']['drupalSettings']['coveo_js']['search_interface'] = $this->config->get('coveo_search_interface');
    $build['#attached']['drupalSettings']['coveo_js']['search_hub'] = $this->config->get('coveo_search_hub');
    $build['#attached']['drupalSettings']['coveo_js']['debug'] = $this->config->get('debug');
    $build['#attached']['drupalSettings']['coveo_js']['search_token'] = $this->coveo->requestSearchToken();
    // Vary cache by URL.
    $build['#cache']['contexts'][] = 'url';
  }

  /**
   * Helper function that returns an array of config keys.
   *
   * @return array
   *   The config keys for a given block.
   */
  protected function getConfigKeys() {
    $keys = [];
    $config = $this->defaultConfiguration();
    if (!empty($config)) {
      $keys = array_keys($config);
    }
    return $keys;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $keys = $this->getConfigKeys();

    if (!empty($keys)) {
      $form['data'] = [
        '#title' => $this->t('Data Attributes'),
        '#type' => 'fieldset',
      ];

      foreach ($keys as $key) {
        $label = ucwords(str_replace('-', ' ', str_replace('data-', '', $key)));
        $form['data'][$key] = [
          '#type' => 'textfield',
          '#title' => $label,
          '#maxlength' => 255,
          '#default_value' => $config[$key],
          '#required' => FALSE,
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $keys = $this->getConfigKeys();

    if (!empty($keys)) {
      foreach ($keys as $key) {
        $this->configuration[$key] = $form_state->getValue(['data', $key]);
      }
    }

    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

}

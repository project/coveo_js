<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Analytics tag.
 *
 * @Block(
 *   id = "coveo_block_analytics",
 *   admin_label = @Translation("Coveo: Analytics"),
 * )
 */
class CoveoBlockAnalytics extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoAnalytics';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return $this->getSearchHubConfiguration();
  }

  /**
   * Helper function that returns the search hub from the module configuration.
   *
   * @return array
   *   Search hub configuration.
   */
  protected function getSearchHubConfiguration() {
    $coveo_search_hub_configuration = [];
    $config = $this->config->get();
    if (isset($config['coveo_search_hub'])) {
      $coveo_search_hub_configuration['data-search-hub'] = $config['coveo_search_hub'];
    }

    return $coveo_search_hub_configuration;
  }

}

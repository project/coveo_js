<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Pager tag.
 *
 * @Block(
 *   id = "coveo_block_pager",
 *   admin_label = @Translation("Coveo: Pager"),
 * )
 */
class CoveoBlockPager extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoPager';

}

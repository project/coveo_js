<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Sort tag.
 *
 * @Block(
 *   id = "coveo_block_sort",
 *   admin_label = @Translation("Coveo: Sort"),
 * )
 */
class CoveoBlockSort extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoSort';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'data-sort-criteria' => "date descending,date ascending",
      'data-caption' => "Date",
    ];
  }

}

<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Results Layout Selector tag.
 *
 * @Block(
 *   id = "coveo_block_results_layout_selector",
 *   admin_label = @Translation("Coveo: Results Layout Selector"),
 * )
 */
class CoveoBlockResultsLayoutSelector extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoResultLayoutSelector';

}

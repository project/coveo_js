<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Query Duration tag.
 *
 * @Block(
 *   id = "coveo_block_query_duration",
 *   admin_label = @Translation("Coveo: Query Duration"),
 * )
 */
class CoveoBlockQueryDuration extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoQueryDuration';

}

<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Share Query tag.
 *
 * @Block(
 *   id = "coveo_block_share_query",
 *   admin_label = @Translation("Coveo: Share Query"),
 * )
 */
class CoveoBlockShareQuery extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoShareQuery';

}

<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Error Report tag.
 *
 * @Block(
 *   id = "coveo_block_error_report",
 *   admin_label = @Translation("Coveo: Error Report"),
 * )
 */
class CoveoBlockErrorReport extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoErrorReport';

}

<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Standalone Search Box tag.
 *
 * @Block(
 *   id = "coveo_block_standalone_search_box",
 *   admin_label = @Translation("Coveo: Standalone Search Box"),
 * )
 */
class CoveoBlockStandaloneSearchBox extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoSearchbox';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'data-enable-reveal-query-suggest-addon' => 'true',
      'data-results-page' => '/search',
    ];
  }

}

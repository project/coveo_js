<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Breadcrumb tag.
 *
 * @Block(
 *   id = "coveo_block_breadcrumb",
 *   admin_label = @Translation("Coveo: Breadcrumb"),
 * )
 */
class CoveoBlockBreadcrumb extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoBreadcrumb';

}

<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Preferences Panel tag.
 *
 * @Block(
 *   id = "coveo_block_preferences_panel",
 *   admin_label = @Translation("Coveo: Preferences Panel"),
 * )
 */
class CoveoBlockPreferencesPanel extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoPreferencesPanel';

}

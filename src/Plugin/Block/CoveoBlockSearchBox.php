<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Search Box tag.
 *
 * @Block(
 *   id = "coveo_block_search_box",
 *   admin_label = @Translation("Coveo: Search Box"),
 * )
 */
class CoveoBlockSearchBox extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoSearchbox';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'data-enable-reveal-query-suggest-addon' => 'true',
      'data-enable-omnibox' => 'true',
    ];
  }

}

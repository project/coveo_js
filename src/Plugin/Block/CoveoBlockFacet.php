<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Facet tag.
 *
 * @Block(
 *   id = "coveo_block_facet",
 *   admin_label = @Translation("Coveo: Facet"),
 * )
 */
class CoveoBlockFacet extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoFacet';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'data-title' => "Author",
      'data-field' => "@author",
    ];
  }

}

<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Dynamic Facet Manager tag.
 *
 * @Block(
 *   id = "coveo_block_dynamic_facet_manager",
 *   admin_label = @Translation("Coveo: Dynamic Facet Manager"),
 * )
 */
class CoveoBlockDynamicFacetManager extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoDynamicFacetManager';

}

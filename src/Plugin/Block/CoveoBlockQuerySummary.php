<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Query Summary tag.
 *
 * @Block(
 *   id = "coveo_block_query_summary",
 *   admin_label = @Translation("Coveo: Query Summary"),
 * )
 */
class CoveoBlockQuerySummary extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoQuerySummary';

}

<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Omni Box tag.
 *
 * @Block(
 *   id = "coveo_block_omni_box",
 *   admin_label = @Translation("Coveo: Omni Box"),
 * )
 */
class CoveoBlockOmniBox extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoOmnibox';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'data-enable-query-suggest-addon' => 'true',
      'data-enable-search-as-you-type' => 'true',
      'data-search-as-you-type-delay' => '10',
      'data-query-suggest-character-threshold' => '3',
      'data-clear-filters-on-new-query' => 'false',
    ];
  }

}

<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Dynamic Facet tag.
 *
 * @Block(
 *   id = "coveo_block_dyamic_facet",
 *   admin_label = @Translation("Coveo: Dynamic Facet"),
 * )
 */
class CoveoBlockDynamicFacet extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoDynamicFacet';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'id' => '',
      'data-depends-on' => '',
      'data-title' => 'Facet Title',
      'data-field' => '@facet_type',
      'data-sort-criteria' => 'score',
      'data-number-of-values' => 10,
    ];
  }

}

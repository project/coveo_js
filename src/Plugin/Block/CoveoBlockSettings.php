<?php

namespace Drupal\coveo_js\Plugin\Block;

/**
 * Provides a block with the Coveo Settings tag.
 *
 * @Block(
 *   id = "coveo_block_settings",
 *   admin_label = @Translation("Coveo: Settings"),
 * )
 */
class CoveoBlockSettings extends CoveoBlockBase {

  /**
   * {@inheritdoc}
   */
  protected $class = 'CoveoSettings';

}

<?php

namespace Drupal\coveo_js\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Coveo JS settings form.
 *
 * @package Drupal\coveo_js\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['coveo_js.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'coveo_js_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('coveo_js.settings');

    $form['coveo_organization_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Coveo organization ID'),
      '#default_value' => $config->get('coveo_organization_id'),
    ];
    $form['coveo_search_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Coveo Search API Key used to request search tokens'),
      '#default_value' => $config->get('coveo_search_api_key'),
    ];
    $form['coveo_search_hub'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Coveo search hub'),
      '#description' => $this->t('Search hub to use.'),
      '#default_value' => $config->get('coveo_search_hub'),
    ];
    $form['coveo_token_expiration'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Coveo token expiration'),
      '#description' => $this->t('The number of milliseconds the search token will remain valid for once it has been created. Min: 900000 (15min) Max: 86400000 (24hrs).'),
      '#default_value' => $config->get('coveo_token_expiration'),
    ];
    $form['coveo_search_interface'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Coveo search interface'),
      '#description' => $this->t('The selector that contains the search interface (result set, pager, facets, etc). For example, <strong>body.path-search</strong>'),
      '#default_value' => $config->get('coveo_search_interface'),
    ];
    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging mode'),
      '#default_value' => $config->get('debug'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('coveo_js.settings');
    $values = $form_state->getValues();
    $expiration = (int) $values['coveo_token_expiration'];
    $expiration = ($expiration < 900000) ? 900000 : (($expiration > 86400000) ? 86400000 : $expiration);
    $config->set('coveo_organization_id', $values['coveo_organization_id'])
      ->set('coveo_search_api_key', $values['coveo_search_api_key'])
      ->set('coveo_search_hub', $values['coveo_search_hub'])
      ->set('coveo_token_expiration', $expiration)
      ->set('coveo_search_interface', $values['coveo_search_interface'])
      ->set('debug', $values['debug'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}

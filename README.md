# Coveo JS

## Contents of this file

- Introduction
- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers
## Introduction

This module is a lightweight wrapper to the [Coveo JavaScript Search Framework](https://docs.coveo.com/en/375/javascript-search-framework/use-the-javascript-search-framework-classic).
It provides the ability to store the API key required, and generate an authentication
token. It provides the necessary Drupal blocks for building out the search UI.

## Requirements

This requires a Coveo API key, with permissions as noted [here](https://docs.coveo.com/en/56/build-a-search-ui/use-search-token-authentication).

The [Custom Meta module](https://www.drupal.org/project/custom_meta) is
recommended for passing custom facet data to the Coveo crawler pipeline, for
any data not supported by conventional meta tags.

## Installation

Install as you would normally install a contributed Drupal module.

### Configure Coveo JS

- Navigate to Admin > Config > Search > Coveo JS
- On the same page, fill out the API key, organization name, and other configuration data

### Configure the Search UI via core Drupal block system

- Coveo blocks can be placed at `/search*` Admin > Structure > Block layout

### Configure the Search UI via a twig template

As an alternative to the above block-based layout, overriding the template for
the `/search` route can be done. For details on building out this HTML, refer
to the [Coveo JS UI documentation](https://coveo.github.io/search-ui/globals.html).

## Troubleshooting


## Maintainers

Current maintainers:
- Jonathan Hedstrom ([@jhedstrom](https://www.drupal.org/u/jhedstrom))
